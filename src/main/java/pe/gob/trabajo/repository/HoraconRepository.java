package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Horacon;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Horacon entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HoraconRepository extends JpaRepository<Horacon, Long> {

    @Query("select H from Horacon H " +
    "where H.nFlgactivo = 1 ")
    List<Horacon> ListarHorasActivas();

    @Query("select H from Horacon H " +
    "where H.nFlgactivo = 1  and H.id not in " +
    "(Select C.horacon.id from Cancohorfec C where " + 
    "to_date(cast(C.dFecconci as date)) =  to_date(?1,'DD/MM/YYYY') and C.nFlgactivo = 1 " + 
    "and C.nCantcon = (Select P.nCantcon from Perporhor P " + 
    "where to_date(?1,'DD/MM/YYYY') between  to_date(cast(P.dFecinicio as date)) " + 
    "and to_date(cast(P.dFecfin as date)) and P.nFlgactivo = 1 )) ")
    List<Horacon> ListarHorasActivasFecha(String fecha);
}
