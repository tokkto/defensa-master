package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Dettipprov;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Dettipprov entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DettipprovRepository extends JpaRepository<Dettipprov, Long> {
    @Query("select D from Dettipprov D " +
    "where D.nFlgactivo = 1 and D.tipproveid.id = ?1")
    List<Dettipprov> ListarDettipprovActivosPorTipproveid(Long id);
}
