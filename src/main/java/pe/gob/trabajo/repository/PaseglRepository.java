package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Pasegl;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.sql.Date;
import java.sql.Time;
import java.time.Instant;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Pasegl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaseglRepository extends JpaRepository<Pasegl, Long> {

    @Query("select P from Pasegl P " +
    "where  " +
    "P.id in ( select P1.id from Pasegl P1 " +
             " where P1.atencion.datlab.trabajador.pernatural.vNumdoc = ?2 and P1.atencion.datlab.trabajador.pernatural.tipdocident.id = ?1 " +
             " and P1.atencion.datlab.empleador.perjuridica.nFlgactivo = 1 and P1.nFlgactivo = 1 and P.atencion.nFlgactivo = 1 and P1.atencion.datlab.nFlgactivo = 1 " +
             " and P1.vEstado = 1  and P1.oficina.id = 2 ) or " +
    "P.id in ( select P2.id from Pasegl P2 " +
             " where P2.atencion.datlab.empleador.pernatural.vNumdoc = ?2 and P2.atencion.datlab.empleador.pernatural.tipdocident.id = ?1 " +
             " and P2.atencion.datlab.empleador.perjuridica.nFlgactivo = 1 and P2.nFlgactivo = 1 and P2.atencion.nFlgactivo = 1 and P2.atencion.datlab.nFlgactivo = 1 " + 
             "  and P2.vEstado = 1  and P2.oficina.id = 2 ) or " +
    "P.id in ( select P3.id from Pasegl P3 " +
             " where P3.atencion.datlab.empleador.perjuridica.vNumdoc = ?2 " +
             " and P3.atencion.datlab.empleador.perjuridica.nFlgactivo = 1 and P3.nFlgactivo = 1 and P3.atencion.nFlgactivo = 1 and P3.atencion.datlab.nFlgactivo = 1 " + 
             "  and P3.vEstado = 1 and P3.oficina.id = 2) " )
    List<Pasegl>findPaseDocumentoParam(Long tip_doc, String nro_doc);

    @Query("select P from Pasegl P " +
    "where P.tFecreg between ?1  and ?2 and P.vEstado = 1  and P.oficina.id = 2 " +
    "and P.nFlgactivo = 1 ")
    List<Pasegl>findPaseFechaParamInstant(Instant fec_ini, Instant fec_fin);

    @Query("select P from Pasegl P " +
    "where to_date(cast(P.tFecreg as date)) between to_date(?1,'DD/MM/YYYY')  and to_date(?2,'DD/MM/YYYY') " +
    "and P.nFlgactivo = 1 and P.vEstado = 1  and P.oficina.id = 2 ")
    List<Pasegl>findPaseFechaParam(String fec_ini, String fec_fin);

    @Query("select P from Pasegl P " +
    "where P.id = ?1 " +
    "and P.nFlgactivo = 1 ")
    Pasegl findPaseId(Long id_pase);
}
