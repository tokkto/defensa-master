package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Estexpedien;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Estexpedien entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EstexpedienRepository extends JpaRepository<Estexpedien, Long> {

}
