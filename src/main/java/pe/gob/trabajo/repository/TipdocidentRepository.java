package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipdocident;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipdocident entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipdocidentRepository extends JpaRepository<Tipdocident, Long> {
    
    @Query("select T from Tipdocident T " +
    "where T.nFlgactivo = 1 ")
    List<Tipdocident> findCombotipIdent();

    @Query("select T from Tipdocident T " +
    "where T.nFlgactivo = 1 and T.vDescorta != ?1 " +
    "order by T.id asc")
    List<Tipdocident> findCombotipIdentExcl(String des_cor);

}
