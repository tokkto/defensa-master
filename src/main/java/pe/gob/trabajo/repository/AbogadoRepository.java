package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Abogado;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Abogado entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AbogadoRepository extends JpaRepository<Abogado, Long> {
    
    @Query("select A from Abogado A " +
    "where A.nFlgactivo = 1 ")
    List<Abogado> ListarAbogadosActivos();

}
