package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Direcnotif;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Direcnotif entity.
 */
public interface DirecnotifSearchRepository extends ElasticsearchRepository<Direcnotif, Long> {
}
