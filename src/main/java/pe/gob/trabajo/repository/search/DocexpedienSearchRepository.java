package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Docexpedien;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Docexpedien entity.
 */
public interface DocexpedienSearchRepository extends ElasticsearchRepository<Docexpedien, Long> {
}
