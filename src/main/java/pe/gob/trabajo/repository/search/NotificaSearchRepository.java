package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Notifica;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Notifica entity.
 */
public interface NotificaSearchRepository extends ElasticsearchRepository<Notifica, Long> {
}
