package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Estexpedien;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Estexpedien entity.
 */
public interface EstexpedienSearchRepository extends ElasticsearchRepository<Estexpedien, Long> {
}
