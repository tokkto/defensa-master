package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipnotif;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipnotif entity.
 */
public interface TipnotifSearchRepository extends ElasticsearchRepository<Tipnotif, Long> {
}
