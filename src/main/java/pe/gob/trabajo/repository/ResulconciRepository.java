package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Resulconci;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Resulconci entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResulconciRepository extends JpaRepository<Resulconci, Long> {

    @Query("select R from Resulconci R " +
    "where R.nFlgactivo = 1 ")
    List<Resulconci> findResConcTot();

    @Query("select R from Resulconci R " +
    "where R.nFlgactivo = 1 and R.vDescrip like %?1%")
    List<Resulconci> findResConcDesc(String desc);

}
