package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipdocexp;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipdocexp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipdocexpRepository extends JpaRepository<Tipdocexp, Long> {
    @Query("select T from Tipdocexp T " +
    "where T.nFlgactivo = 1 ")
    List<Tipdocexp> ListarTipdocExpActivos();
}
