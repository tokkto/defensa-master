package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Dettipprov;

import pe.gob.trabajo.repository.DettipprovRepository;
import pe.gob.trabajo.repository.search.DettipprovSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Dettipprov.
 */
@RestController
@RequestMapping("/api")
public class DettipprovResource {

    private final Logger log = LoggerFactory.getLogger(DettipprovResource.class);

    private static final String ENTITY_NAME = "dettipprov";

    private final DettipprovRepository dettipprovRepository;

    private final DettipprovSearchRepository dettipprovSearchRepository;

    public DettipprovResource(DettipprovRepository dettipprovRepository, DettipprovSearchRepository dettipprovSearchRepository) {
        this.dettipprovRepository = dettipprovRepository;
        this.dettipprovSearchRepository = dettipprovSearchRepository;
    }

    /**
     * POST  /dettipprovs : Create a new dettipprov.
     *
     * @param dettipprov the dettipprov to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dettipprov, or with status 400 (Bad Request) if the dettipprov has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/dettipprovs")
    @Timed
    public ResponseEntity<Dettipprov> createDettipprov(@Valid @RequestBody Dettipprov dettipprov) throws URISyntaxException {
        log.debug("REST request to save Dettipprov : {}", dettipprov);
        if (dettipprov.getId() != null) {
            throw new BadRequestAlertException("A new dettipprov cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Dettipprov result = dettipprovRepository.save(dettipprov);
        dettipprovSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/dettipprovs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /dettipprovs : Updates an existing dettipprov.
     *
     * @param dettipprov the dettipprov to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dettipprov,
     * or with status 400 (Bad Request) if the dettipprov is not valid,
     * or with status 500 (Internal Server Error) if the dettipprov couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/dettipprovs")
    @Timed
    public ResponseEntity<Dettipprov> updateDettipprov(@Valid @RequestBody Dettipprov dettipprov) throws URISyntaxException {
        log.debug("REST request to update Dettipprov : {}", dettipprov);
        if (dettipprov.getId() == null) {
            return createDettipprov(dettipprov);
        }
        Dettipprov result = dettipprovRepository.save(dettipprov);
        dettipprovSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dettipprov.getId().toString()))
            .body(result);
    }

    /**
     * GET  /dettipprovs : get all the dettipprovs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dettipprovs in body
     */
    @GetMapping("/dettipprovs")
    @Timed
    public List<Dettipprov> getAllDettipprovs() {
        log.debug("REST request to get all Dettipprovs");
        return dettipprovRepository.findAll();
    }

    @GetMapping("/dettipprovs/tipproveid/{id}")
    @Timed
    public List<Dettipprov> getAllDettipprovsPorTipproveid(@PathVariable Long id) {
        log.debug("REST request to get all Dettipprovs");
        return dettipprovRepository.ListarDettipprovActivosPorTipproveid(id);
    }

    /**
     * GET  /dettipprovs/:id : get the "id" dettipprov.
     *
     * @param id the id of the dettipprov to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dettipprov, or with status 404 (Not Found)
     */
    @GetMapping("/dettipprovs/{id}")
    @Timed
    public ResponseEntity<Dettipprov> getDettipprov(@PathVariable Long id) {
        log.debug("REST request to get Dettipprov : {}", id);
        Dettipprov dettipprov = dettipprovRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dettipprov));
    }

    /**
     * DELETE  /dettipprovs/:id : delete the "id" dettipprov.
     *
     * @param id the id of the dettipprov to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/dettipprovs/{id}")
    @Timed
    public ResponseEntity<Void> deleteDettipprov(@PathVariable Long id) {
        log.debug("REST request to delete Dettipprov : {}", id);
        dettipprovRepository.delete(id);
        dettipprovSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/dettipprovs?query=:query : search for the dettipprov corresponding
     * to the query.
     *
     * @param query the query of the dettipprov search
     * @return the result of the search
     */
    @GetMapping("/_search/dettipprovs")
    @Timed
    public List<Dettipprov> searchDettipprovs(@RequestParam String query) {
        log.debug("REST request to search Dettipprovs for query {}", query);
        return StreamSupport
            .stream(dettipprovSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
