package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Cancohorfec;

import pe.gob.trabajo.repository.CancohorfecRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;


/**
 * REST controller for managing Abogado.
 */
@RestController
@RequestMapping("/api")
public class CancohorfecResource {

    private final Logger log = LoggerFactory.getLogger(PerporhorResource.class);

    private static final String ENTITY_NAME = "cancohorfec";

    private final CancohorfecRepository cancohorfecRepository;

    //private final AbogadoSearchRepository abogadoSearchRepository;

    public CancohorfecResource(CancohorfecRepository cancohorfecRepository) {
        this.cancohorfecRepository = cancohorfecRepository;
    }

    /**
     * POST  /abogados : Create a new abogado.
     *
     * @param cancohorfec the abogado to create
     * @return the ResponseEntity with status 201 (Created) and with body the new abogado, or with status 400 (Bad Request) if the abogado has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/cancohorfecs")
    @Timed
    public ResponseEntity<Cancohorfec> createCancohorfec(@Valid @RequestBody Cancohorfec cancohorfec) throws URISyntaxException {
        log.debug("REST request to save Cancohorfecs : {}", cancohorfec);
        if (cancohorfec.getId() != null) {
            throw new BadRequestAlertException("A new abogado cannot already have an ID", ENTITY_NAME, "idexists");
        }
        cancohorfec.tFecreg(Instant.now());
        cancohorfec.nSedereg(1);
        cancohorfec.nUsuareg(1);
        Cancohorfec result = cancohorfecRepository.save(cancohorfec);
        return ResponseEntity.created(new URI("/api/cancohorfecs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /abogados : Updates an existing abogado.
     *
     * @param cancohorfec the abogado to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated abogado,
     * or with status 400 (Bad Request) if the abogado is not valid,
     * or with status 500 (Internal Server Error) if the abogado couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/cancohorfecs")
    @Timed
    public ResponseEntity<Cancohorfec> updateCancohorfec(@Valid @RequestBody Cancohorfec cancohorfec) throws URISyntaxException {
        log.debug("REST request to update Cancohorfecs : {}", cancohorfec);
        if (cancohorfec.getId() == null) {
            return createCancohorfec(cancohorfec);
        }
        Cancohorfec result = cancohorfecRepository.save(cancohorfec);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, cancohorfec.getId().toString()))
            .body(result);
    }

    /**
     * GET  /abogados : get all the abogados.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of abogados in body
     */
    @GetMapping("/cancohorfecs")
    @Timed
    public List<Cancohorfec> getAllCancohorfecs() {
        log.debug("REST request to get all Cancohorfecs");
        return cancohorfecRepository.findAll();
        }

    /**
     * GET  /abogados/:id : get the "id" abogado.
     *
     * @param id the id of the abogado to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the abogado, or with status 404 (Not Found)
     */
    @GetMapping("/cancohorfecs/{id}")
    @Timed
    public ResponseEntity<Cancohorfec> getCancohorfec(@PathVariable Long id) {
        log.debug("REST request to get Cancohorfec : {}", id);
        Cancohorfec cancohorfec = cancohorfecRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(cancohorfec));
    }

    /**
     * DELETE  /abogados/:id : delete the "id" abogado.
     *
     * @param id the id of the abogado to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/cancohorfecs/{id}")
    @Timed
    public ResponseEntity<Void> deleteCancohorfec(@PathVariable Long id) {
        log.debug("REST request to delete Cancohorfec : {}", id);
        cancohorfecRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
