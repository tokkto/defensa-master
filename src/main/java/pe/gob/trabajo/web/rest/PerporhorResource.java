package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Perporhor;

import pe.gob.trabajo.repository.PerporhorRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Abogado.
 */
@RestController
@RequestMapping("/api")
public class PerporhorResource {

    private final Logger log = LoggerFactory.getLogger(PerporhorResource.class);

    private static final String ENTITY_NAME = "perporhor";

    private final PerporhorRepository perporhorRepository;

    public PerporhorResource(PerporhorRepository perporhorRepository) {
        this.perporhorRepository = perporhorRepository;
    }

    /**
     * POST  /abogados : Create a new abogado.
     *
     * @param perporhor the abogado to create
     * @return the ResponseEntity with status 201 (Created) and with body the new abogado, or with status 400 (Bad Request) if the abogado has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/perporhors")
    @Timed
    public ResponseEntity<Perporhor> createPerporhor(@Valid @RequestBody Perporhor perporhor) throws URISyntaxException {
        log.debug("REST request to save Perporhor : {}", perporhor);
        if (perporhor.getId() != null) {
            throw new BadRequestAlertException("A new abogado cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Perporhor result = perporhorRepository.save(perporhor);
        return ResponseEntity.created(new URI("/api/perporhors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /abogados : Updates an existing abogado.
     *
     * @param perporhor the abogado to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated abogado,
     * or with status 400 (Bad Request) if the abogado is not valid,
     * or with status 500 (Internal Server Error) if the abogado couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/perporhors")
    @Timed
    public ResponseEntity<Perporhor> updatePerporhor(@Valid @RequestBody Perporhor perporhor) throws URISyntaxException {
        log.debug("REST request to update Perporhor : {}", perporhor);
        if (perporhor.getId() == null) {
            return createPerporhor(perporhor);
        }
        Perporhor result = perporhorRepository.save(perporhor);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, perporhor.getId().toString()))
            .body(result);
    }

    /**
     * GET  /abogados : get all the abogados.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of abogados in body
     */
    @GetMapping("/perporhors")
    @Timed
    public List<Perporhor> getAllPerporhor() {
        log.debug("REST request to get all Perporhor");
        return perporhorRepository.findAll();
        }

    /**
     * GET  /abogados/:id : get the "id" abogado.
     *
     * @param id the id of the abogado to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the abogado, or with status 404 (Not Found)
     */
    @GetMapping("/perporhors/{id}")
    @Timed
    public ResponseEntity<Perporhor> getPerporhor(@PathVariable Long id) {
        log.debug("REST request to get Perporhor : {}", id);
        Perporhor perporhor = perporhorRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(perporhor));
    }

    /**
     * DELETE  /abogados/:id : delete the "id" abogado.
     *
     * @param id the id of the abogado to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/perporhors/{id}")
    @Timed
    public ResponseEntity<Void> deletePerporhor(@PathVariable Long id) {
        log.debug("REST request to delete Perporhor : {}", id);
        perporhorRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
