package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipproveid;

import pe.gob.trabajo.repository.TipproveidRepository;
import pe.gob.trabajo.repository.search.TipproveidSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipproveid.
 */
@RestController
@RequestMapping("/api")
public class TipproveidResource {

    private final Logger log = LoggerFactory.getLogger(TipproveidResource.class);

    private static final String ENTITY_NAME = "tipproveid";

    private final TipproveidRepository tipproveidRepository;

    private final TipproveidSearchRepository tipproveidSearchRepository;

    public TipproveidResource(TipproveidRepository tipproveidRepository, TipproveidSearchRepository tipproveidSearchRepository) {
        this.tipproveidRepository = tipproveidRepository;
        this.tipproveidSearchRepository = tipproveidSearchRepository;
    }

    /**
     * POST  /tipproveids : Create a new tipproveid.
     *
     * @param tipproveid the tipproveid to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipproveid, or with status 400 (Bad Request) if the tipproveid has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipproveids")
    @Timed
    public ResponseEntity<Tipproveid> createTipproveid(@Valid @RequestBody Tipproveid tipproveid) throws URISyntaxException {
        log.debug("REST request to save Tipproveid : {}", tipproveid);
        if (tipproveid.getId() != null) {
            throw new BadRequestAlertException("A new tipproveid cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tipproveid result = tipproveidRepository.save(tipproveid);
        tipproveidSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipproveids/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipproveids : Updates an existing tipproveid.
     *
     * @param tipproveid the tipproveid to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipproveid,
     * or with status 400 (Bad Request) if the tipproveid is not valid,
     * or with status 500 (Internal Server Error) if the tipproveid couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipproveids")
    @Timed
    public ResponseEntity<Tipproveid> updateTipproveid(@Valid @RequestBody Tipproveid tipproveid) throws URISyntaxException {
        log.debug("REST request to update Tipproveid : {}", tipproveid);
        if (tipproveid.getId() == null) {
            return createTipproveid(tipproveid);
        }
        Tipproveid result = tipproveidRepository.save(tipproveid);
        tipproveidSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipproveid.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipproveids : get all the tipproveids.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipproveids in body
     */
    @GetMapping("/tipproveids")
    @Timed
    public List<Tipproveid> getAllTipproveids() {
        log.debug("REST request to get all Tipproveids");
        return tipproveidRepository.findAll();
        }

    @GetMapping("/tipproveids/tipdocexp/{id}")
    @Timed
    public List<Tipproveid> getTipproveidTipdocExp(@PathVariable Long id) {
        log.debug("REST request to get Tipproveid : {}", id);
        return tipproveidRepository.ListarTipproveidActivosPorTDocExp(id);
    }

    /**
     * GET  /tipproveids/:id : get the "id" tipproveid.
     *
     * @param id the id of the tipproveid to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipproveid, or with status 404 (Not Found)
     */
    @GetMapping("/tipproveids/{id}")
    @Timed
    public ResponseEntity<Tipproveid> getTipproveid(@PathVariable Long id) {
        log.debug("REST request to get Tipproveid : {}", id);
        Tipproveid tipproveid = tipproveidRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipproveid));
    }
  
    /**
     * DELETE  /tipproveids/:id : delete the "id" tipproveid.
     *
     * @param id the id of the tipproveid to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipproveids/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipproveid(@PathVariable Long id) {
        log.debug("REST request to delete Tipproveid : {}", id);
        tipproveidRepository.delete(id);
        tipproveidSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipproveids?query=:query : search for the tipproveid corresponding
     * to the query.
     *
     * @param query the query of the tipproveid search
     * @return the result of the search
     */
    @GetMapping("/_search/tipproveids")
    @Timed
    public List<Tipproveid> searchTipproveids(@RequestParam String query) {
        log.debug("REST request to search Tipproveids for query {}", query);
        return StreamSupport
            .stream(tipproveidSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
