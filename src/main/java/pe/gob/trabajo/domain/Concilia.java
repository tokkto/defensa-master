package pe.gob.trabajo.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Defensa Legal
 */
@ApiModel(description = "Defensa Legal")
@Entity
@Table(name = "COMVC_CONCILIA")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "concilia")
public class Concilia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codconci", nullable = false)
    private Long id;

    @Column(name = "d_fecconci")
    private LocalDate dFecconci;

    @Column(name = "n_flgaudres")
    private Boolean nFlgaudres;

    @Column(name = "n_flgreprog")
    private Boolean nFlgreprog;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codexp")
    private Expediente expediente;

    @ManyToOne
    @JoinColumn(name = "n_codabogad")
    private Abogado abogado;

    @ManyToOne
    @JoinColumn(name = "n_codhora")
    private Horacon horacon;

    @ManyToOne
    @JoinColumn(name = "n_codrescon")
    private Resulconci resulconci;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getdFecconci() {
        return dFecconci;
    }

    public Concilia dFecconci(LocalDate dFecconci) {
        this.dFecconci = dFecconci;
        return this;
    }

    public void setdFecconci(LocalDate dFecconci) {
        this.dFecconci = dFecconci;
    }

    public Boolean isnFlgaudres() {
        return nFlgaudres;
    }

    public Concilia nFlgaudres(Boolean nFlgaudres) {
        this.nFlgaudres = nFlgaudres;
        return this;
    }

    public void setnFlgaudres(Boolean nFlgaudres) {
        this.nFlgaudres = nFlgaudres;
    }

    public Boolean isnFlgreprog() {
        return nFlgreprog;
    }

    public Concilia nFlgreprog(Boolean nFlgreprog) {
        this.nFlgreprog = nFlgreprog;
        return this;
    }

    public void setnFlgreprog(Boolean nFlgreprog) {
        this.nFlgreprog = nFlgreprog;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Concilia nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Concilia tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Concilia nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Concilia nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Concilia nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Concilia tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Concilia nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public Concilia expediente(Expediente expediente) {
        this.expediente = expediente;
        return this;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Abogado getAbogado() {
        return abogado;
    }

    public Concilia abogado(Abogado abogado) {
        this.abogado = abogado;
        return this;
    }

    public void setAbogado(Abogado abogado) {
        this.abogado = abogado;
    }

    public Horacon getHoracon() {
        return horacon;
    }

    public Concilia horacon(Horacon horacon) {
        this.horacon = horacon;
        return this;
    }

    public void setHoracon(Horacon horacon) {
        this.horacon = horacon;
    }

    public Resulconci getResulconci() {
        return resulconci;
    }

    public Concilia resulconci(Resulconci resulconci) {
        this.resulconci = resulconci;
        return this;
    }

    public void setResulconci(Resulconci resulconci) {
        this.resulconci = resulconci;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Concilia concilia = (Concilia) o;
        if (concilia.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), concilia.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Concilia{" +
            "id=" + getId() +
            ", dFecconci='" + getdFecconci() + "'" +
            ", nFlgaudres='" + isnFlgaudres() + "'" +
            ", nFlgreprog='" + isnFlgreprog() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
