package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tipproveid.
 */
@Entity
@Table(name = "DLTBC_TIPPROVEID")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tipproveid")
public class Tipproveid implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codtiprov", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "v_descrip", length = 100, nullable = false)
    private String vDescrip;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codtdocex")
    private Tipdocexp tipdocexp;

    @OneToMany(mappedBy = "tipproveid")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Dettipprov> dettipprovs = new HashSet<>();

    @OneToMany(mappedBy = "tipproveid")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Docexpedien> docexpediens = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvDescrip() {
        return vDescrip;
    }

    public Tipproveid vDescrip(String vDescrip) {
        this.vDescrip = vDescrip;
        return this;
    }

    public void setvDescrip(String vDescrip) {
        this.vDescrip = vDescrip;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Tipproveid nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Tipproveid tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Tipproveid nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Tipproveid nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Tipproveid nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Tipproveid tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Tipproveid nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Tipdocexp getTipdocexp() {
        return tipdocexp;
    }

    public Tipproveid tipdocexp(Tipdocexp tipdocexp) {
        this.tipdocexp = tipdocexp;
        return this;
    }

    public void setTipdocexp(Tipdocexp tipdocexp) {
        this.tipdocexp = tipdocexp;
    }

    public Set<Dettipprov> getDettipprovs() {
        return dettipprovs;
    }

    public Tipproveid dettipprovs(Set<Dettipprov> dettipprovs) {
        this.dettipprovs = dettipprovs;
        return this;
    }

    public Tipproveid addDettipprov(Dettipprov dettipprov) {
        this.dettipprovs.add(dettipprov);
        dettipprov.setTipproveid(this);
        return this;
    }

    public Tipproveid removeDettipprov(Dettipprov dettipprov) {
        this.dettipprovs.remove(dettipprov);
        dettipprov.setTipproveid(null);
        return this;
    }

    public void setDettipprovs(Set<Dettipprov> dettipprovs) {
        this.dettipprovs = dettipprovs;
    }

    public Set<Docexpedien> getDocexpediens() {
        return docexpediens;
    }

    public Tipproveid docexpediens(Set<Docexpedien> docexpediens) {
        this.docexpediens = docexpediens;
        return this;
    }

    public Tipproveid addDocexpedien(Docexpedien docexpedien) {
        this.docexpediens.add(docexpedien);
        docexpedien.setTipproveid(this);
        return this;
    }

    public Tipproveid removeDocexpedien(Docexpedien docexpedien) {
        this.docexpediens.remove(docexpedien);
        docexpedien.setTipproveid(null);
        return this;
    }

    public void setDocexpediens(Set<Docexpedien> docexpediens) {
        this.docexpediens = docexpediens;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tipproveid tipproveid = (Tipproveid) o;
        if (tipproveid.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipproveid.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tipproveid{" +
            "id=" + getId() +
            ", vDescrip='" + getvDescrip() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
