package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Multaconci.
 */
@Entity
@Table(name = "DLMVC_MULTACONCI")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "multaconci")
public class Multaconci implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codmulta", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "f_monmulta", nullable = false)
    private Float fMonmulta;

    @Size(max = 20)
    @Column(name = "v_numresosd", length = 20)
    private String vNumresosd;

    @Column(name = "d_fecresosd")
    private LocalDate dFecresosd;

    @Size(max = 20)
    @Column(name = "vcodigo", length = 20)
    private String vcodigo;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    private Resolucrd resolucrd;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getfMonmulta() {
        return fMonmulta;
    }

    public Multaconci fMonmulta(Float fMonmulta) {
        this.fMonmulta = fMonmulta;
        return this;
    }

    public void setfMonmulta(Float fMonmulta) {
        this.fMonmulta = fMonmulta;
    }

    public String getvNumresosd() {
        return vNumresosd;
    }

    public Multaconci vNumresosd(String vNumresosd) {
        this.vNumresosd = vNumresosd;
        return this;
    }

    public void setvNumresosd(String vNumresosd) {
        this.vNumresosd = vNumresosd;
    }

    public LocalDate getdFecresosd() {
        return dFecresosd;
    }

    public Multaconci dFecresosd(LocalDate dFecresosd) {
        this.dFecresosd = dFecresosd;
        return this;
    }

    public void setdFecresosd(LocalDate dFecresosd) {
        this.dFecresosd = dFecresosd;
    }

    public String getVcodigo() {
        return vcodigo;
    }

    public Multaconci vcodigo(String vcodigo) {
        this.vcodigo = vcodigo;
        return this;
    }

    public void setVcodigo(String vcodigo) {
        this.vcodigo = vcodigo;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Multaconci nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Multaconci tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Multaconci nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Multaconci nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Multaconci nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Multaconci tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Multaconci nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Resolucrd getResolucrd() {
        return resolucrd;
    }

    public Multaconci resolucrd(Resolucrd resolucrd) {
        this.resolucrd = resolucrd;
        return this;
    }

    public void setResolucrd(Resolucrd resolucrd) {
        this.resolucrd = resolucrd;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Multaconci multaconci = (Multaconci) o;
        if (multaconci.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), multaconci.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Multaconci{" +
            "id=" + getId() +
            ", fMonmulta='" + getfMonmulta() + "'" +
            ", vNumresosd='" + getvNumresosd() + "'" +
            ", dFecresosd='" + getdFecresosd() + "'" +
            ", vcodigo='" + getVcodigo() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
