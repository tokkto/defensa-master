package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Tipnotif;
import pe.gob.trabajo.repository.TipnotifRepository;
import pe.gob.trabajo.repository.search.TipnotifSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipnotifResource REST controller.
 *
 * @see TipnotifResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class TipnotifResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipnotifRepository tipnotifRepository;

    @Autowired
    private TipnotifSearchRepository tipnotifSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipnotifMockMvc;

    private Tipnotif tipnotif;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipnotifResource tipnotifResource = new TipnotifResource(tipnotifRepository, tipnotifSearchRepository);
        this.restTipnotifMockMvc = MockMvcBuilders.standaloneSetup(tipnotifResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipnotif createEntity(EntityManager em) {
        Tipnotif tipnotif = new Tipnotif()
            .vDescrip(DEFAULT_V_DESCRIP)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipnotif;
    }

    @Before
    public void initTest() {
        tipnotifSearchRepository.deleteAll();
        tipnotif = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipnotif() throws Exception {
        int databaseSizeBeforeCreate = tipnotifRepository.findAll().size();

        // Create the Tipnotif
        restTipnotifMockMvc.perform(post("/api/tipnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipnotif)))
            .andExpect(status().isCreated());

        // Validate the Tipnotif in the database
        List<Tipnotif> tipnotifList = tipnotifRepository.findAll();
        assertThat(tipnotifList).hasSize(databaseSizeBeforeCreate + 1);
        Tipnotif testTipnotif = tipnotifList.get(tipnotifList.size() - 1);
        assertThat(testTipnotif.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testTipnotif.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testTipnotif.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipnotif.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipnotif.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipnotif.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testTipnotif.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipnotif.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipnotif in Elasticsearch
        Tipnotif tipnotifEs = tipnotifSearchRepository.findOne(testTipnotif.getId());
        assertThat(tipnotifEs).isEqualToComparingFieldByField(testTipnotif);
    }

    @Test
    @Transactional
    public void createTipnotifWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipnotifRepository.findAll().size();

        // Create the Tipnotif with an existing ID
        tipnotif.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipnotifMockMvc.perform(post("/api/tipnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipnotif)))
            .andExpect(status().isBadRequest());

        // Validate the Tipnotif in the database
        List<Tipnotif> tipnotifList = tipnotifRepository.findAll();
        assertThat(tipnotifList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipnotifRepository.findAll().size();
        // set the field null
        tipnotif.setvDescrip(null);

        // Create the Tipnotif, which fails.

        restTipnotifMockMvc.perform(post("/api/tipnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipnotif)))
            .andExpect(status().isBadRequest());

        List<Tipnotif> tipnotifList = tipnotifRepository.findAll();
        assertThat(tipnotifList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipnotifRepository.findAll().size();
        // set the field null
        tipnotif.setnUsuareg(null);

        // Create the Tipnotif, which fails.

        restTipnotifMockMvc.perform(post("/api/tipnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipnotif)))
            .andExpect(status().isBadRequest());

        List<Tipnotif> tipnotifList = tipnotifRepository.findAll();
        assertThat(tipnotifList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipnotifRepository.findAll().size();
        // set the field null
        tipnotif.settFecreg(null);

        // Create the Tipnotif, which fails.

        restTipnotifMockMvc.perform(post("/api/tipnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipnotif)))
            .andExpect(status().isBadRequest());

        List<Tipnotif> tipnotifList = tipnotifRepository.findAll();
        assertThat(tipnotifList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipnotifRepository.findAll().size();
        // set the field null
        tipnotif.setnFlgactivo(null);

        // Create the Tipnotif, which fails.

        restTipnotifMockMvc.perform(post("/api/tipnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipnotif)))
            .andExpect(status().isBadRequest());

        List<Tipnotif> tipnotifList = tipnotifRepository.findAll();
        assertThat(tipnotifList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipnotifRepository.findAll().size();
        // set the field null
        tipnotif.setnSedereg(null);

        // Create the Tipnotif, which fails.

        restTipnotifMockMvc.perform(post("/api/tipnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipnotif)))
            .andExpect(status().isBadRequest());

        List<Tipnotif> tipnotifList = tipnotifRepository.findAll();
        assertThat(tipnotifList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipnotifs() throws Exception {
        // Initialize the database
        tipnotifRepository.saveAndFlush(tipnotif);

        // Get all the tipnotifList
        restTipnotifMockMvc.perform(get("/api/tipnotifs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipnotif.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipnotif() throws Exception {
        // Initialize the database
        tipnotifRepository.saveAndFlush(tipnotif);

        // Get the tipnotif
        restTipnotifMockMvc.perform(get("/api/tipnotifs/{id}", tipnotif.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipnotif.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipnotif() throws Exception {
        // Get the tipnotif
        restTipnotifMockMvc.perform(get("/api/tipnotifs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipnotif() throws Exception {
        // Initialize the database
        tipnotifRepository.saveAndFlush(tipnotif);
        tipnotifSearchRepository.save(tipnotif);
        int databaseSizeBeforeUpdate = tipnotifRepository.findAll().size();

        // Update the tipnotif
        Tipnotif updatedTipnotif = tipnotifRepository.findOne(tipnotif.getId());
        updatedTipnotif
            .vDescrip(UPDATED_V_DESCRIP)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipnotifMockMvc.perform(put("/api/tipnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipnotif)))
            .andExpect(status().isOk());

        // Validate the Tipnotif in the database
        List<Tipnotif> tipnotifList = tipnotifRepository.findAll();
        assertThat(tipnotifList).hasSize(databaseSizeBeforeUpdate);
        Tipnotif testTipnotif = tipnotifList.get(tipnotifList.size() - 1);
        assertThat(testTipnotif.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testTipnotif.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testTipnotif.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipnotif.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipnotif.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipnotif.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testTipnotif.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipnotif.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipnotif in Elasticsearch
        Tipnotif tipnotifEs = tipnotifSearchRepository.findOne(testTipnotif.getId());
        assertThat(tipnotifEs).isEqualToComparingFieldByField(testTipnotif);
    }

    @Test
    @Transactional
    public void updateNonExistingTipnotif() throws Exception {
        int databaseSizeBeforeUpdate = tipnotifRepository.findAll().size();

        // Create the Tipnotif

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipnotifMockMvc.perform(put("/api/tipnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipnotif)))
            .andExpect(status().isCreated());

        // Validate the Tipnotif in the database
        List<Tipnotif> tipnotifList = tipnotifRepository.findAll();
        assertThat(tipnotifList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipnotif() throws Exception {
        // Initialize the database
        tipnotifRepository.saveAndFlush(tipnotif);
        tipnotifSearchRepository.save(tipnotif);
        int databaseSizeBeforeDelete = tipnotifRepository.findAll().size();

        // Get the tipnotif
        restTipnotifMockMvc.perform(delete("/api/tipnotifs/{id}", tipnotif.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipnotifExistsInEs = tipnotifSearchRepository.exists(tipnotif.getId());
        assertThat(tipnotifExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipnotif> tipnotifList = tipnotifRepository.findAll();
        assertThat(tipnotifList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipnotif() throws Exception {
        // Initialize the database
        tipnotifRepository.saveAndFlush(tipnotif);
        tipnotifSearchRepository.save(tipnotif);

        // Search the tipnotif
        restTipnotifMockMvc.perform(get("/api/_search/tipnotifs?query=id:" + tipnotif.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipnotif.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipnotif.class);
        Tipnotif tipnotif1 = new Tipnotif();
        tipnotif1.setId(1L);
        Tipnotif tipnotif2 = new Tipnotif();
        tipnotif2.setId(tipnotif1.getId());
        assertThat(tipnotif1).isEqualTo(tipnotif2);
        tipnotif2.setId(2L);
        assertThat(tipnotif1).isNotEqualTo(tipnotif2);
        tipnotif1.setId(null);
        assertThat(tipnotif1).isNotEqualTo(tipnotif2);
    }
}
