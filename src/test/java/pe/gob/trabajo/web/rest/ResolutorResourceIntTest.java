package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Resolutor;
import pe.gob.trabajo.repository.ResolutorRepository;
import pe.gob.trabajo.repository.search.ResolutorSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ResolutorResource REST controller.
 *
 * @see ResolutorResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class ResolutorResourceIntTest {

    private static final String DEFAULT_V_NOMRESOL = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMRESOL = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_CODRESUSU = 1;
    private static final Integer UPDATED_N_CODRESUSU = 2;

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private ResolutorRepository resolutorRepository;

    @Autowired
    private ResolutorSearchRepository resolutorSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restResolutorMockMvc;

    private Resolutor resolutor;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ResolutorResource resolutorResource = new ResolutorResource(resolutorRepository, resolutorSearchRepository);
        this.restResolutorMockMvc = MockMvcBuilders.standaloneSetup(resolutorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Resolutor createEntity(EntityManager em) {
        Resolutor resolutor = new Resolutor()
            .vNomresol(DEFAULT_V_NOMRESOL)
            .nCodresusu(DEFAULT_N_CODRESUSU)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return resolutor;
    }

    @Before
    public void initTest() {
        resolutorSearchRepository.deleteAll();
        resolutor = createEntity(em);
    }

    @Test
    @Transactional
    public void createResolutor() throws Exception {
        int databaseSizeBeforeCreate = resolutorRepository.findAll().size();

        // Create the Resolutor
        restResolutorMockMvc.perform(post("/api/resolutors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolutor)))
            .andExpect(status().isCreated());

        // Validate the Resolutor in the database
        List<Resolutor> resolutorList = resolutorRepository.findAll();
        assertThat(resolutorList).hasSize(databaseSizeBeforeCreate + 1);
        Resolutor testResolutor = resolutorList.get(resolutorList.size() - 1);
        assertThat(testResolutor.getvNomresol()).isEqualTo(DEFAULT_V_NOMRESOL);
        assertThat(testResolutor.getnCodresusu()).isEqualTo(DEFAULT_N_CODRESUSU);
        assertThat(testResolutor.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testResolutor.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testResolutor.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testResolutor.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testResolutor.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testResolutor.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testResolutor.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Resolutor in Elasticsearch
        Resolutor resolutorEs = resolutorSearchRepository.findOne(testResolutor.getId());
        assertThat(resolutorEs).isEqualToComparingFieldByField(testResolutor);
    }

    @Test
    @Transactional
    public void createResolutorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resolutorRepository.findAll().size();

        // Create the Resolutor with an existing ID
        resolutor.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResolutorMockMvc.perform(post("/api/resolutors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolutor)))
            .andExpect(status().isBadRequest());

        // Validate the Resolutor in the database
        List<Resolutor> resolutorList = resolutorRepository.findAll();
        assertThat(resolutorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvNomresolIsRequired() throws Exception {
        int databaseSizeBeforeTest = resolutorRepository.findAll().size();
        // set the field null
        resolutor.setvNomresol(null);

        // Create the Resolutor, which fails.

        restResolutorMockMvc.perform(post("/api/resolutors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolutor)))
            .andExpect(status().isBadRequest());

        List<Resolutor> resolutorList = resolutorRepository.findAll();
        assertThat(resolutorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = resolutorRepository.findAll().size();
        // set the field null
        resolutor.setnUsuareg(null);

        // Create the Resolutor, which fails.

        restResolutorMockMvc.perform(post("/api/resolutors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolutor)))
            .andExpect(status().isBadRequest());

        List<Resolutor> resolutorList = resolutorRepository.findAll();
        assertThat(resolutorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = resolutorRepository.findAll().size();
        // set the field null
        resolutor.settFecreg(null);

        // Create the Resolutor, which fails.

        restResolutorMockMvc.perform(post("/api/resolutors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolutor)))
            .andExpect(status().isBadRequest());

        List<Resolutor> resolutorList = resolutorRepository.findAll();
        assertThat(resolutorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = resolutorRepository.findAll().size();
        // set the field null
        resolutor.setnFlgactivo(null);

        // Create the Resolutor, which fails.

        restResolutorMockMvc.perform(post("/api/resolutors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolutor)))
            .andExpect(status().isBadRequest());

        List<Resolutor> resolutorList = resolutorRepository.findAll();
        assertThat(resolutorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = resolutorRepository.findAll().size();
        // set the field null
        resolutor.setnSedereg(null);

        // Create the Resolutor, which fails.

        restResolutorMockMvc.perform(post("/api/resolutors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolutor)))
            .andExpect(status().isBadRequest());

        List<Resolutor> resolutorList = resolutorRepository.findAll();
        assertThat(resolutorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllResolutors() throws Exception {
        // Initialize the database
        resolutorRepository.saveAndFlush(resolutor);

        // Get all the resolutorList
        restResolutorMockMvc.perform(get("/api/resolutors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resolutor.getId().intValue())))
            .andExpect(jsonPath("$.[*].vNomresol").value(hasItem(DEFAULT_V_NOMRESOL.toString())))
            .andExpect(jsonPath("$.[*].nCodresusu").value(hasItem(DEFAULT_N_CODRESUSU)))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getResolutor() throws Exception {
        // Initialize the database
        resolutorRepository.saveAndFlush(resolutor);

        // Get the resolutor
        restResolutorMockMvc.perform(get("/api/resolutors/{id}", resolutor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(resolutor.getId().intValue()))
            .andExpect(jsonPath("$.vNomresol").value(DEFAULT_V_NOMRESOL.toString()))
            .andExpect(jsonPath("$.nCodresusu").value(DEFAULT_N_CODRESUSU))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingResolutor() throws Exception {
        // Get the resolutor
        restResolutorMockMvc.perform(get("/api/resolutors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResolutor() throws Exception {
        // Initialize the database
        resolutorRepository.saveAndFlush(resolutor);
        resolutorSearchRepository.save(resolutor);
        int databaseSizeBeforeUpdate = resolutorRepository.findAll().size();

        // Update the resolutor
        Resolutor updatedResolutor = resolutorRepository.findOne(resolutor.getId());
        updatedResolutor
            .vNomresol(UPDATED_V_NOMRESOL)
            .nCodresusu(UPDATED_N_CODRESUSU)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restResolutorMockMvc.perform(put("/api/resolutors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedResolutor)))
            .andExpect(status().isOk());

        // Validate the Resolutor in the database
        List<Resolutor> resolutorList = resolutorRepository.findAll();
        assertThat(resolutorList).hasSize(databaseSizeBeforeUpdate);
        Resolutor testResolutor = resolutorList.get(resolutorList.size() - 1);
        assertThat(testResolutor.getvNomresol()).isEqualTo(UPDATED_V_NOMRESOL);
        assertThat(testResolutor.getnCodresusu()).isEqualTo(UPDATED_N_CODRESUSU);
        assertThat(testResolutor.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testResolutor.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testResolutor.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testResolutor.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testResolutor.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testResolutor.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testResolutor.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Resolutor in Elasticsearch
        Resolutor resolutorEs = resolutorSearchRepository.findOne(testResolutor.getId());
        assertThat(resolutorEs).isEqualToComparingFieldByField(testResolutor);
    }

    @Test
    @Transactional
    public void updateNonExistingResolutor() throws Exception {
        int databaseSizeBeforeUpdate = resolutorRepository.findAll().size();

        // Create the Resolutor

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restResolutorMockMvc.perform(put("/api/resolutors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolutor)))
            .andExpect(status().isCreated());

        // Validate the Resolutor in the database
        List<Resolutor> resolutorList = resolutorRepository.findAll();
        assertThat(resolutorList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteResolutor() throws Exception {
        // Initialize the database
        resolutorRepository.saveAndFlush(resolutor);
        resolutorSearchRepository.save(resolutor);
        int databaseSizeBeforeDelete = resolutorRepository.findAll().size();

        // Get the resolutor
        restResolutorMockMvc.perform(delete("/api/resolutors/{id}", resolutor.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean resolutorExistsInEs = resolutorSearchRepository.exists(resolutor.getId());
        assertThat(resolutorExistsInEs).isFalse();

        // Validate the database is empty
        List<Resolutor> resolutorList = resolutorRepository.findAll();
        assertThat(resolutorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchResolutor() throws Exception {
        // Initialize the database
        resolutorRepository.saveAndFlush(resolutor);
        resolutorSearchRepository.save(resolutor);

        // Search the resolutor
        restResolutorMockMvc.perform(get("/api/_search/resolutors?query=id:" + resolutor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resolutor.getId().intValue())))
            .andExpect(jsonPath("$.[*].vNomresol").value(hasItem(DEFAULT_V_NOMRESOL.toString())))
            .andExpect(jsonPath("$.[*].nCodresusu").value(hasItem(DEFAULT_N_CODRESUSU)))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Resolutor.class);
        Resolutor resolutor1 = new Resolutor();
        resolutor1.setId(1L);
        Resolutor resolutor2 = new Resolutor();
        resolutor2.setId(resolutor1.getId());
        assertThat(resolutor1).isEqualTo(resolutor2);
        resolutor2.setId(2L);
        assertThat(resolutor1).isNotEqualTo(resolutor2);
        resolutor1.setId(null);
        assertThat(resolutor1).isNotEqualTo(resolutor2);
    }
}
