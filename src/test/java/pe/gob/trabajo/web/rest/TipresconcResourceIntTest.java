package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Tipresconc;
import pe.gob.trabajo.repository.TipresconcRepository;
import pe.gob.trabajo.repository.search.TipresconcSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipresconcResource REST controller.
 *
 * @see TipresconcResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class TipresconcResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipresconcRepository tipresconcRepository;

    @Autowired
    private TipresconcSearchRepository tipresconcSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipresconcMockMvc;

    private Tipresconc tipresconc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipresconcResource tipresconcResource = new TipresconcResource(tipresconcRepository, tipresconcSearchRepository);
        this.restTipresconcMockMvc = MockMvcBuilders.standaloneSetup(tipresconcResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipresconc createEntity(EntityManager em) {
        Tipresconc tipresconc = new Tipresconc()
            .vDescrip(DEFAULT_V_DESCRIP)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipresconc;
    }

    @Before
    public void initTest() {
        tipresconcSearchRepository.deleteAll();
        tipresconc = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipresconc() throws Exception {
        int databaseSizeBeforeCreate = tipresconcRepository.findAll().size();

        // Create the Tipresconc
        restTipresconcMockMvc.perform(post("/api/tipresconcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresconc)))
            .andExpect(status().isCreated());

        // Validate the Tipresconc in the database
        List<Tipresconc> tipresconcList = tipresconcRepository.findAll();
        assertThat(tipresconcList).hasSize(databaseSizeBeforeCreate + 1);
        Tipresconc testTipresconc = tipresconcList.get(tipresconcList.size() - 1);
        assertThat(testTipresconc.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testTipresconc.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testTipresconc.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipresconc.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipresconc.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipresconc.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testTipresconc.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipresconc.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipresconc in Elasticsearch
        Tipresconc tipresconcEs = tipresconcSearchRepository.findOne(testTipresconc.getId());
        assertThat(tipresconcEs).isEqualToComparingFieldByField(testTipresconc);
    }

    @Test
    @Transactional
    public void createTipresconcWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipresconcRepository.findAll().size();

        // Create the Tipresconc with an existing ID
        tipresconc.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipresconcMockMvc.perform(post("/api/tipresconcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresconc)))
            .andExpect(status().isBadRequest());

        // Validate the Tipresconc in the database
        List<Tipresconc> tipresconcList = tipresconcRepository.findAll();
        assertThat(tipresconcList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipresconcRepository.findAll().size();
        // set the field null
        tipresconc.setvDescrip(null);

        // Create the Tipresconc, which fails.

        restTipresconcMockMvc.perform(post("/api/tipresconcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresconc)))
            .andExpect(status().isBadRequest());

        List<Tipresconc> tipresconcList = tipresconcRepository.findAll();
        assertThat(tipresconcList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipresconcRepository.findAll().size();
        // set the field null
        tipresconc.setnUsuareg(null);

        // Create the Tipresconc, which fails.

        restTipresconcMockMvc.perform(post("/api/tipresconcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresconc)))
            .andExpect(status().isBadRequest());

        List<Tipresconc> tipresconcList = tipresconcRepository.findAll();
        assertThat(tipresconcList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipresconcRepository.findAll().size();
        // set the field null
        tipresconc.settFecreg(null);

        // Create the Tipresconc, which fails.

        restTipresconcMockMvc.perform(post("/api/tipresconcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresconc)))
            .andExpect(status().isBadRequest());

        List<Tipresconc> tipresconcList = tipresconcRepository.findAll();
        assertThat(tipresconcList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipresconcRepository.findAll().size();
        // set the field null
        tipresconc.setnFlgactivo(null);

        // Create the Tipresconc, which fails.

        restTipresconcMockMvc.perform(post("/api/tipresconcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresconc)))
            .andExpect(status().isBadRequest());

        List<Tipresconc> tipresconcList = tipresconcRepository.findAll();
        assertThat(tipresconcList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipresconcRepository.findAll().size();
        // set the field null
        tipresconc.setnSedereg(null);

        // Create the Tipresconc, which fails.

        restTipresconcMockMvc.perform(post("/api/tipresconcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresconc)))
            .andExpect(status().isBadRequest());

        List<Tipresconc> tipresconcList = tipresconcRepository.findAll();
        assertThat(tipresconcList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipresconcs() throws Exception {
        // Initialize the database
        tipresconcRepository.saveAndFlush(tipresconc);

        // Get all the tipresconcList
        restTipresconcMockMvc.perform(get("/api/tipresconcs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipresconc.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipresconc() throws Exception {
        // Initialize the database
        tipresconcRepository.saveAndFlush(tipresconc);

        // Get the tipresconc
        restTipresconcMockMvc.perform(get("/api/tipresconcs/{id}", tipresconc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipresconc.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipresconc() throws Exception {
        // Get the tipresconc
        restTipresconcMockMvc.perform(get("/api/tipresconcs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipresconc() throws Exception {
        // Initialize the database
        tipresconcRepository.saveAndFlush(tipresconc);
        tipresconcSearchRepository.save(tipresconc);
        int databaseSizeBeforeUpdate = tipresconcRepository.findAll().size();

        // Update the tipresconc
        Tipresconc updatedTipresconc = tipresconcRepository.findOne(tipresconc.getId());
        updatedTipresconc
            .vDescrip(UPDATED_V_DESCRIP)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipresconcMockMvc.perform(put("/api/tipresconcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipresconc)))
            .andExpect(status().isOk());

        // Validate the Tipresconc in the database
        List<Tipresconc> tipresconcList = tipresconcRepository.findAll();
        assertThat(tipresconcList).hasSize(databaseSizeBeforeUpdate);
        Tipresconc testTipresconc = tipresconcList.get(tipresconcList.size() - 1);
        assertThat(testTipresconc.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testTipresconc.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testTipresconc.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipresconc.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipresconc.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipresconc.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testTipresconc.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipresconc.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipresconc in Elasticsearch
        Tipresconc tipresconcEs = tipresconcSearchRepository.findOne(testTipresconc.getId());
        assertThat(tipresconcEs).isEqualToComparingFieldByField(testTipresconc);
    }

    @Test
    @Transactional
    public void updateNonExistingTipresconc() throws Exception {
        int databaseSizeBeforeUpdate = tipresconcRepository.findAll().size();

        // Create the Tipresconc

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipresconcMockMvc.perform(put("/api/tipresconcs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipresconc)))
            .andExpect(status().isCreated());

        // Validate the Tipresconc in the database
        List<Tipresconc> tipresconcList = tipresconcRepository.findAll();
        assertThat(tipresconcList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipresconc() throws Exception {
        // Initialize the database
        tipresconcRepository.saveAndFlush(tipresconc);
        tipresconcSearchRepository.save(tipresconc);
        int databaseSizeBeforeDelete = tipresconcRepository.findAll().size();

        // Get the tipresconc
        restTipresconcMockMvc.perform(delete("/api/tipresconcs/{id}", tipresconc.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipresconcExistsInEs = tipresconcSearchRepository.exists(tipresconc.getId());
        assertThat(tipresconcExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipresconc> tipresconcList = tipresconcRepository.findAll();
        assertThat(tipresconcList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipresconc() throws Exception {
        // Initialize the database
        tipresconcRepository.saveAndFlush(tipresconc);
        tipresconcSearchRepository.save(tipresconc);

        // Search the tipresconc
        restTipresconcMockMvc.perform(get("/api/_search/tipresconcs?query=id:" + tipresconc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipresconc.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipresconc.class);
        Tipresconc tipresconc1 = new Tipresconc();
        tipresconc1.setId(1L);
        Tipresconc tipresconc2 = new Tipresconc();
        tipresconc2.setId(tipresconc1.getId());
        assertThat(tipresconc1).isEqualTo(tipresconc2);
        tipresconc2.setId(2L);
        assertThat(tipresconc1).isNotEqualTo(tipresconc2);
        tipresconc1.setId(null);
        assertThat(tipresconc1).isNotEqualTo(tipresconc2);
    }
}
