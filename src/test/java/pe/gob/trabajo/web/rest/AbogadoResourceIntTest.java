package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Abogado;
import pe.gob.trabajo.repository.AbogadoRepository;
import pe.gob.trabajo.repository.search.AbogadoSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AbogadoResource REST controller.
 *
 * @see AbogadoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class AbogadoResourceIntTest {

    private static final String DEFAULT_V_NOMABOGAD = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMABOGAD = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_CODABOUSU = 1;
    private static final Integer UPDATED_N_CODABOUSU = 2;

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private AbogadoRepository abogadoRepository;

    @Autowired
    private AbogadoSearchRepository abogadoSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAbogadoMockMvc;

    private Abogado abogado;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AbogadoResource abogadoResource = new AbogadoResource(abogadoRepository, abogadoSearchRepository);
        this.restAbogadoMockMvc = MockMvcBuilders.standaloneSetup(abogadoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Abogado createEntity(EntityManager em) {
        Abogado abogado = new Abogado()
            .vNomabogad(DEFAULT_V_NOMABOGAD)
            .nCodabousu(DEFAULT_N_CODABOUSU)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return abogado;
    }

    @Before
    public void initTest() {
        abogadoSearchRepository.deleteAll();
        abogado = createEntity(em);
    }

    @Test
    @Transactional
    public void createAbogado() throws Exception {
        int databaseSizeBeforeCreate = abogadoRepository.findAll().size();

        // Create the Abogado
        restAbogadoMockMvc.perform(post("/api/abogados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(abogado)))
            .andExpect(status().isCreated());

        // Validate the Abogado in the database
        List<Abogado> abogadoList = abogadoRepository.findAll();
        assertThat(abogadoList).hasSize(databaseSizeBeforeCreate + 1);
        Abogado testAbogado = abogadoList.get(abogadoList.size() - 1);
        assertThat(testAbogado.getvNomabogad()).isEqualTo(DEFAULT_V_NOMABOGAD);
        assertThat(testAbogado.getnCodabousu()).isEqualTo(DEFAULT_N_CODABOUSU);
        assertThat(testAbogado.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testAbogado.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testAbogado.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testAbogado.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testAbogado.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testAbogado.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testAbogado.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Abogado in Elasticsearch
        Abogado abogadoEs = abogadoSearchRepository.findOne(testAbogado.getId());
        assertThat(abogadoEs).isEqualToComparingFieldByField(testAbogado);
    }

    @Test
    @Transactional
    public void createAbogadoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = abogadoRepository.findAll().size();

        // Create the Abogado with an existing ID
        abogado.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAbogadoMockMvc.perform(post("/api/abogados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(abogado)))
            .andExpect(status().isBadRequest());

        // Validate the Abogado in the database
        List<Abogado> abogadoList = abogadoRepository.findAll();
        assertThat(abogadoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = abogadoRepository.findAll().size();
        // set the field null
        abogado.setnUsuareg(null);

        // Create the Abogado, which fails.

        restAbogadoMockMvc.perform(post("/api/abogados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(abogado)))
            .andExpect(status().isBadRequest());

        List<Abogado> abogadoList = abogadoRepository.findAll();
        assertThat(abogadoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = abogadoRepository.findAll().size();
        // set the field null
        abogado.settFecreg(null);

        // Create the Abogado, which fails.

        restAbogadoMockMvc.perform(post("/api/abogados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(abogado)))
            .andExpect(status().isBadRequest());

        List<Abogado> abogadoList = abogadoRepository.findAll();
        assertThat(abogadoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = abogadoRepository.findAll().size();
        // set the field null
        abogado.setnFlgactivo(null);

        // Create the Abogado, which fails.

        restAbogadoMockMvc.perform(post("/api/abogados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(abogado)))
            .andExpect(status().isBadRequest());

        List<Abogado> abogadoList = abogadoRepository.findAll();
        assertThat(abogadoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = abogadoRepository.findAll().size();
        // set the field null
        abogado.setnSedereg(null);

        // Create the Abogado, which fails.

        restAbogadoMockMvc.perform(post("/api/abogados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(abogado)))
            .andExpect(status().isBadRequest());

        List<Abogado> abogadoList = abogadoRepository.findAll();
        assertThat(abogadoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAbogados() throws Exception {
        // Initialize the database
        abogadoRepository.saveAndFlush(abogado);

        // Get all the abogadoList
        restAbogadoMockMvc.perform(get("/api/abogados?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(abogado.getId().intValue())))
            .andExpect(jsonPath("$.[*].vNomabogad").value(hasItem(DEFAULT_V_NOMABOGAD.toString())))
            .andExpect(jsonPath("$.[*].nCodabousu").value(hasItem(DEFAULT_N_CODABOUSU)))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getAbogado() throws Exception {
        // Initialize the database
        abogadoRepository.saveAndFlush(abogado);

        // Get the abogado
        restAbogadoMockMvc.perform(get("/api/abogados/{id}", abogado.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(abogado.getId().intValue()))
            .andExpect(jsonPath("$.vNomabogad").value(DEFAULT_V_NOMABOGAD.toString()))
            .andExpect(jsonPath("$.nCodabousu").value(DEFAULT_N_CODABOUSU))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingAbogado() throws Exception {
        // Get the abogado
        restAbogadoMockMvc.perform(get("/api/abogados/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAbogado() throws Exception {
        // Initialize the database
        abogadoRepository.saveAndFlush(abogado);
        abogadoSearchRepository.save(abogado);
        int databaseSizeBeforeUpdate = abogadoRepository.findAll().size();

        // Update the abogado
        Abogado updatedAbogado = abogadoRepository.findOne(abogado.getId());
        updatedAbogado
            .vNomabogad(UPDATED_V_NOMABOGAD)
            .nCodabousu(UPDATED_N_CODABOUSU)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restAbogadoMockMvc.perform(put("/api/abogados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAbogado)))
            .andExpect(status().isOk());

        // Validate the Abogado in the database
        List<Abogado> abogadoList = abogadoRepository.findAll();
        assertThat(abogadoList).hasSize(databaseSizeBeforeUpdate);
        Abogado testAbogado = abogadoList.get(abogadoList.size() - 1);
        assertThat(testAbogado.getvNomabogad()).isEqualTo(UPDATED_V_NOMABOGAD);
        assertThat(testAbogado.getnCodabousu()).isEqualTo(UPDATED_N_CODABOUSU);
        assertThat(testAbogado.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testAbogado.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testAbogado.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testAbogado.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testAbogado.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testAbogado.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testAbogado.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Abogado in Elasticsearch
        Abogado abogadoEs = abogadoSearchRepository.findOne(testAbogado.getId());
        assertThat(abogadoEs).isEqualToComparingFieldByField(testAbogado);
    }

    @Test
    @Transactional
    public void updateNonExistingAbogado() throws Exception {
        int databaseSizeBeforeUpdate = abogadoRepository.findAll().size();

        // Create the Abogado

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAbogadoMockMvc.perform(put("/api/abogados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(abogado)))
            .andExpect(status().isCreated());

        // Validate the Abogado in the database
        List<Abogado> abogadoList = abogadoRepository.findAll();
        assertThat(abogadoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAbogado() throws Exception {
        // Initialize the database
        abogadoRepository.saveAndFlush(abogado);
        abogadoSearchRepository.save(abogado);
        int databaseSizeBeforeDelete = abogadoRepository.findAll().size();

        // Get the abogado
        restAbogadoMockMvc.perform(delete("/api/abogados/{id}", abogado.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean abogadoExistsInEs = abogadoSearchRepository.exists(abogado.getId());
        assertThat(abogadoExistsInEs).isFalse();

        // Validate the database is empty
        List<Abogado> abogadoList = abogadoRepository.findAll();
        assertThat(abogadoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAbogado() throws Exception {
        // Initialize the database
        abogadoRepository.saveAndFlush(abogado);
        abogadoSearchRepository.save(abogado);

        // Search the abogado
        restAbogadoMockMvc.perform(get("/api/_search/abogados?query=id:" + abogado.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(abogado.getId().intValue())))
            .andExpect(jsonPath("$.[*].vNomabogad").value(hasItem(DEFAULT_V_NOMABOGAD.toString())))
            .andExpect(jsonPath("$.[*].nCodabousu").value(hasItem(DEFAULT_N_CODABOUSU)))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Abogado.class);
        Abogado abogado1 = new Abogado();
        abogado1.setId(1L);
        Abogado abogado2 = new Abogado();
        abogado2.setId(abogado1.getId());
        assertThat(abogado1).isEqualTo(abogado2);
        abogado2.setId(2L);
        assertThat(abogado1).isNotEqualTo(abogado2);
        abogado1.setId(null);
        assertThat(abogado1).isNotEqualTo(abogado2);
    }
}
