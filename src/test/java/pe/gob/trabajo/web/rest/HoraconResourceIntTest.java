package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Horacon;
import pe.gob.trabajo.repository.HoraconRepository;
import pe.gob.trabajo.repository.search.HoraconSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the HoraconResource REST controller.
 *
 * @see HoraconResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class HoraconResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private HoraconRepository horaconRepository;

    @Autowired
    private HoraconSearchRepository horaconSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restHoraconMockMvc;

    private Horacon horacon;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HoraconResource horaconResource = new HoraconResource(horaconRepository, horaconSearchRepository);
        this.restHoraconMockMvc = MockMvcBuilders.standaloneSetup(horaconResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Horacon createEntity(EntityManager em) {
        Horacon horacon = new Horacon()
            .vDescrip(DEFAULT_V_DESCRIP)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return horacon;
    }

    @Before
    public void initTest() {
        horaconSearchRepository.deleteAll();
        horacon = createEntity(em);
    }

    @Test
    @Transactional
    public void createHoracon() throws Exception {
        int databaseSizeBeforeCreate = horaconRepository.findAll().size();

        // Create the Horacon
        restHoraconMockMvc.perform(post("/api/horacons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horacon)))
            .andExpect(status().isCreated());

        // Validate the Horacon in the database
        List<Horacon> horaconList = horaconRepository.findAll();
        assertThat(horaconList).hasSize(databaseSizeBeforeCreate + 1);
        Horacon testHoracon = horaconList.get(horaconList.size() - 1);
        assertThat(testHoracon.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testHoracon.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testHoracon.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testHoracon.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testHoracon.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testHoracon.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testHoracon.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testHoracon.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Horacon in Elasticsearch
        Horacon horaconEs = horaconSearchRepository.findOne(testHoracon.getId());
        assertThat(horaconEs).isEqualToComparingFieldByField(testHoracon);
    }

    @Test
    @Transactional
    public void createHoraconWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = horaconRepository.findAll().size();

        // Create the Horacon with an existing ID
        horacon.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHoraconMockMvc.perform(post("/api/horacons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horacon)))
            .andExpect(status().isBadRequest());

        // Validate the Horacon in the database
        List<Horacon> horaconList = horaconRepository.findAll();
        assertThat(horaconList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = horaconRepository.findAll().size();
        // set the field null
        horacon.setvDescrip(null);

        // Create the Horacon, which fails.

        restHoraconMockMvc.perform(post("/api/horacons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horacon)))
            .andExpect(status().isBadRequest());

        List<Horacon> horaconList = horaconRepository.findAll();
        assertThat(horaconList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = horaconRepository.findAll().size();
        // set the field null
        horacon.setnUsuareg(null);

        // Create the Horacon, which fails.

        restHoraconMockMvc.perform(post("/api/horacons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horacon)))
            .andExpect(status().isBadRequest());

        List<Horacon> horaconList = horaconRepository.findAll();
        assertThat(horaconList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = horaconRepository.findAll().size();
        // set the field null
        horacon.settFecreg(null);

        // Create the Horacon, which fails.

        restHoraconMockMvc.perform(post("/api/horacons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horacon)))
            .andExpect(status().isBadRequest());

        List<Horacon> horaconList = horaconRepository.findAll();
        assertThat(horaconList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = horaconRepository.findAll().size();
        // set the field null
        horacon.setnFlgactivo(null);

        // Create the Horacon, which fails.

        restHoraconMockMvc.perform(post("/api/horacons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horacon)))
            .andExpect(status().isBadRequest());

        List<Horacon> horaconList = horaconRepository.findAll();
        assertThat(horaconList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = horaconRepository.findAll().size();
        // set the field null
        horacon.setnSedereg(null);

        // Create the Horacon, which fails.

        restHoraconMockMvc.perform(post("/api/horacons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horacon)))
            .andExpect(status().isBadRequest());

        List<Horacon> horaconList = horaconRepository.findAll();
        assertThat(horaconList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHoracons() throws Exception {
        // Initialize the database
        horaconRepository.saveAndFlush(horacon);

        // Get all the horaconList
        restHoraconMockMvc.perform(get("/api/horacons?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(horacon.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getHoracon() throws Exception {
        // Initialize the database
        horaconRepository.saveAndFlush(horacon);

        // Get the horacon
        restHoraconMockMvc.perform(get("/api/horacons/{id}", horacon.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(horacon.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingHoracon() throws Exception {
        // Get the horacon
        restHoraconMockMvc.perform(get("/api/horacons/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHoracon() throws Exception {
        // Initialize the database
        horaconRepository.saveAndFlush(horacon);
        horaconSearchRepository.save(horacon);
        int databaseSizeBeforeUpdate = horaconRepository.findAll().size();

        // Update the horacon
        Horacon updatedHoracon = horaconRepository.findOne(horacon.getId());
        updatedHoracon
            .vDescrip(UPDATED_V_DESCRIP)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restHoraconMockMvc.perform(put("/api/horacons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedHoracon)))
            .andExpect(status().isOk());

        // Validate the Horacon in the database
        List<Horacon> horaconList = horaconRepository.findAll();
        assertThat(horaconList).hasSize(databaseSizeBeforeUpdate);
        Horacon testHoracon = horaconList.get(horaconList.size() - 1);
        assertThat(testHoracon.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testHoracon.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testHoracon.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testHoracon.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testHoracon.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testHoracon.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testHoracon.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testHoracon.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Horacon in Elasticsearch
        Horacon horaconEs = horaconSearchRepository.findOne(testHoracon.getId());
        assertThat(horaconEs).isEqualToComparingFieldByField(testHoracon);
    }

    @Test
    @Transactional
    public void updateNonExistingHoracon() throws Exception {
        int databaseSizeBeforeUpdate = horaconRepository.findAll().size();

        // Create the Horacon

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restHoraconMockMvc.perform(put("/api/horacons")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horacon)))
            .andExpect(status().isCreated());

        // Validate the Horacon in the database
        List<Horacon> horaconList = horaconRepository.findAll();
        assertThat(horaconList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteHoracon() throws Exception {
        // Initialize the database
        horaconRepository.saveAndFlush(horacon);
        horaconSearchRepository.save(horacon);
        int databaseSizeBeforeDelete = horaconRepository.findAll().size();

        // Get the horacon
        restHoraconMockMvc.perform(delete("/api/horacons/{id}", horacon.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean horaconExistsInEs = horaconSearchRepository.exists(horacon.getId());
        assertThat(horaconExistsInEs).isFalse();

        // Validate the database is empty
        List<Horacon> horaconList = horaconRepository.findAll();
        assertThat(horaconList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchHoracon() throws Exception {
        // Initialize the database
        horaconRepository.saveAndFlush(horacon);
        horaconSearchRepository.save(horacon);

        // Search the horacon
        restHoraconMockMvc.perform(get("/api/_search/horacons?query=id:" + horacon.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(horacon.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Horacon.class);
        Horacon horacon1 = new Horacon();
        horacon1.setId(1L);
        Horacon horacon2 = new Horacon();
        horacon2.setId(horacon1.getId());
        assertThat(horacon1).isEqualTo(horacon2);
        horacon2.setId(2L);
        assertThat(horacon1).isNotEqualTo(horacon2);
        horacon1.setId(null);
        assertThat(horacon1).isNotEqualTo(horacon2);
    }
}
